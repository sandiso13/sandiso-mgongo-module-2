void main() {
  //I am going to start by storing all the apps from 2012-2021 in an array
  var arr = [
    'FNB',
    'Health ID',
    'TransUnion Dealer Guide',
    'Rapidtargets',
    'Matchy',
    'Plascon',
    'PhraZApp',
    'DStv',
    '.comm Telco Data Visualizer',
    'PriceCheck Mobile',
    'MarkitShare',
    'Nedbank App suite',
    'SnapScan',
    'Kids Aid',
    'Bookly',
    'Gautrain Buddy',
    'SuperSport',
    'SyncMobile',
    'My Belongings',
    'LIVE Inspect',
    'Vigo',
    'Zapper',
    'Rea Vaya',
    'Wildlife tracker',
    'VulaMobile',
    'DStv Now',
    'WumDrop',
    'CPUT Mobile',
    'EskomSePush',
    'M4JAM',
    'Ikhoa',
    'HearZA',
    'Tuta-me',
    'KaChing',
    'Friendly Math Monsters for kingergarten',
    'TransUnion 1Check',
    'OrderIn',
    'EcoSlips',
    'InterGreatMe',
    'Zulzi',
    'Hey Jude',
    'The ORU Social',
    'TouchSA',
    'Pick n Pay Super Animals 2',
    'The TreeApp South Africa',
    'WatIf Health Portal',
    'Awethu Project',
    'Shyft for Standard Bank',
    'Pineaple',
    'Cowa Bunga',
    'Digemy Knowledge Partner and Besmarter',
    'Bestee',
    'The African Cyber Gaming League App(ACGL)',
    'Db Track',
    'Stokfella',
    'Difela Hymns',
    'Xander English 1-20',
    'Ctrl',
    'Khula',
    'ASI Snakes',
    'SI Realities',
    'Lost Defence',
    'Franc',
    'Vula Mobile 2',
    'Matric Live',
    'My Pregnancy journal',
    'LocTransie',
    'Hydra',
    'Bottles',
    'Over',
    'Digger',
    'MO Wahsh',
    'EasyEquities',
    'Examsta',
    'Checkers sixty60',
    'Technishen',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'Green Fingers Mobile',
    'Xitsonga dictionary',
    'Guardian Health',
    'IiDENTIFI app',
    'Hellopay SoftPOS',
    'Guardian Health Platform',
    'Ambani Africa',
    'Murumi',
    'Shyft',
    'Sisa',
    'UniWise',
    'Kazi',
    'Takealot',
    'Rekindle Learning app',
    'Roadsave',
    'Afrihost'
  ];
  arr.sort(); //arranges the apps in alphabetical order
  print(arr);

  var twentySeventeenWinningApp = "Standard bank Shyft";
  var twentyEighteenWinningApp = "Khula";

  print(twentySeventeenWinningApp);
  print(twentyEighteenWinningApp);

  void printNumberofApps(String company) {
    if (company == "MTN") {
      print("The number of apps in $company is 95");
    } else {
      print("The number of Apps in $company is 94");
    }
  }

  printNumberofApps("MTN"); //prints the total number of Apps in MTN
}
