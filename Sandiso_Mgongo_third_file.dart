//I am going to start by constructing a class of the App with the information it should contain
class App {
  var appName;
  var category;
  var year;

  showAppInfo() {
    print("App Name Is : ${appName}");
    print("App Category Is : ${category}");
    print("The Year The App Won Is : ${year}");
  }
}

void main() {
  var app =
      new App(); //here I will declare variables to the names that are contained by the app
  app.appName = "Shyft";
  app.category = "Best Financial Solution";
  app.year = "2021";
  print("App information");
  app.showAppInfo();
  print(app.appName.toUpperCase()); //prints the app name in capital letters
}
