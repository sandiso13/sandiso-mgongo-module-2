//I am going to use variables to store my full name, my favourite app and city
var name = "Sandiso Mgongo";
var favouriteApp = "You Tube";
var favouriteCity = "Cape Town";

//I am now going to print all my variables
void main() {
  print(name);
  print(favouriteApp);
  print(favouriteCity);
}
